package com.quipux.demo.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="reproducciones")
public class ListaDeReproduccion implements Serializable{
	

	private static final long serialVersionUID = 1L;

	@Id
	private String nombre;
	private String descripcion;
	
	@OneToMany(mappedBy = "reproducciones", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Cancion> canciones;
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Cancion> getCanciones() {
		return canciones;
	}

	public void setCanciones(List<Cancion> canciones) {
		this.canciones = canciones;
	}

	@Override
	public String toString() {
		return "ListaDeReproduccion [nombre=" + nombre + ", descripcion=" + descripcion + ", canciones=" + canciones
				+ "]";
	}

	
	
}
