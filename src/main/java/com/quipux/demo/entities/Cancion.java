package com.quipux.demo.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="canciones")
public class Cancion implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	private String titulo;
	private String artista;
	private String album;
	private int anno;
	
	
	 @ManyToOne()
	 @JoinColumn(name = "nombre")
	 private ListaDeReproduccion reproducciones;
	

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getArtista() {
		return artista;
	}
	public void setArtista(String artista) {
		this.artista = artista;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public int getAnno() {
		return anno;
	}
	public void setAnno(int anno) {
		this.anno = anno;
	}
	@Override
	public String toString() {
		return "Cancion [titulo=" + titulo + ", artista=" + artista + ", album=" + album + ", anno=" + anno
				+ ", reproducciones=" + reproducciones + "]";
	}
	
	
	
}
