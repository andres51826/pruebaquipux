package com.quipux.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quipux.demo.dao.ICancionDao;
import com.quipux.demo.dao.IReproduccionesDao;
import com.quipux.demo.entities.Cancion;
import com.quipux.demo.entities.ListaDeReproduccion;
import com.quipux.demo.servicesImpl.IReproduccionesService;

@Service
public class ReproduccionesServiceImpl implements IReproduccionesService{
	
	@Autowired
	private IReproduccionesDao reproduccionesDao;
	
	@Autowired
	private ICancionDao cancionDao;

	@Override
	public List<ListaDeReproduccion> findAll() {
		// TODO Auto-generated method stub
		return (List<ListaDeReproduccion>) reproduccionesDao.findAll();
	}

	@Override
	public ListaDeReproduccion findByName(String name) {
		// TODO Auto-generated method stub
		return reproduccionesDao.findById(name).orElse(null);
	}

	@Override
	public void deleteByName(String name) {
		reproduccionesDao.deleteById(name);
		
	}

	@Override
	public ListaDeReproduccion create(ListaDeReproduccion reproduccion) {
		// TODO Auto-generated method stub
		
		//validamos que el nombre no sea null o venga vacio
		if(reproduccion.getNombre() == null || reproduccion.getNombre().equals("")) {
			return null;
		}
		
		List<Cancion> canciones = reproduccion.getCanciones();
		reproduccion.setCanciones(null);
		
		ListaDeReproduccion reproduccionnew = reproduccionesDao.save(reproduccion);
		
		if(canciones != null) {
			for(Cancion cancion : canciones) {
				cancionDao.crearCancion(cancion.getTitulo(), cancion.getArtista(), cancion.getAlbum(), cancion.getAnno(), reproduccionnew.getNombre());
			}
		}
		reproduccionnew.setCanciones(canciones);
		return reproduccionnew;
	}

}
