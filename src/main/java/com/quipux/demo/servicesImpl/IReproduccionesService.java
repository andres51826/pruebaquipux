package com.quipux.demo.servicesImpl;

import java.util.List;

import com.quipux.demo.entities.ListaDeReproduccion;

public interface IReproduccionesService {
	
	public List<ListaDeReproduccion> findAll();
	public ListaDeReproduccion findByName(String name);
	public void deleteByName(String name);
	public ListaDeReproduccion create(ListaDeReproduccion reproduccion);
}
