package com.quipux.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaQuipux3Application {

	public static void main(String[] args) {
		SpringApplication.run(PruebaQuipux3Application.class, args);
	}
	

}
