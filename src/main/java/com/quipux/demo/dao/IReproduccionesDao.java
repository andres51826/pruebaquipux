package com.quipux.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.quipux.demo.entities.ListaDeReproduccion;

public interface IReproduccionesDao extends JpaRepository<ListaDeReproduccion, String> {

}
