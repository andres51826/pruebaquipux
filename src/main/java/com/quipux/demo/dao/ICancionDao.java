package com.quipux.demo.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.quipux.demo.entities.Cancion;

public interface ICancionDao extends CrudRepository<Cancion, String>{
	
	@Modifying
    @Query(value = "insert into canciones (titulo,artista,album,anno,nombre) VALUES (:titulo,:artista,:album,:anno,:nombre)", nativeQuery = true)
    @Transactional
	public void crearCancion(@Param("titulo") String titulo, @Param("artista") String artista,@Param("album") String album,
			@Param("anno") int anno,@Param("nombre") String nombre);
}
