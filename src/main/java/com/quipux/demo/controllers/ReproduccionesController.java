package com.quipux.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.quipux.demo.entities.ListaDeReproduccion;
import com.quipux.demo.servicesImpl.IReproduccionesService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
public class ReproduccionesController {
	
	@Autowired
	private IReproduccionesService reproduccionesService;

	@GetMapping("/lists")
	public List<ListaDeReproduccion> findAll() {
		return reproduccionesService.findAll();
	}
	
	
	@GetMapping("/lists/{listName}")
	public ResponseEntity<ListaDeReproduccion> findByName(@PathVariable String listName) {
		ListaDeReproduccion reproduccion = reproduccionesService.findByName(listName);
		if(reproduccion == null) {
			return new ResponseEntity<ListaDeReproduccion>(reproduccion,HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<ListaDeReproduccion>(reproduccion,HttpStatus.OK);
		}
	}
	
	
	@PostMapping("/lists")
	public ResponseEntity<ListaDeReproduccion> create(@RequestBody ListaDeReproduccion reproduccion) {
		System.out.println(reproduccion.toString());
		ListaDeReproduccion newReproduccion = reproduccionesService.create(reproduccion);
		if(newReproduccion == null) {
			return new ResponseEntity<ListaDeReproduccion>(newReproduccion,HttpStatus.BAD_REQUEST);
		}else {
			return new ResponseEntity<ListaDeReproduccion>(newReproduccion,HttpStatus.CREATED);
		}
	}
	
	
	@DeleteMapping("/lists/{listName}")
	public ResponseEntity<String> delete(@PathVariable String listName) {
		ListaDeReproduccion reproduccion = reproduccionesService.findByName(listName);
		if(reproduccion == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		reproduccionesService.deleteByName(listName);
		return new ResponseEntity<String>("Delete",HttpStatus.NO_CONTENT);
	}
	
}
